FROM node:20-bullseye-slim
WORKDIR /app
COPY . .
RUN npm install
EXPOSE 80
CMD ["node", "index.js"]

